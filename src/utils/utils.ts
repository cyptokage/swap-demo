import { BigNumber } from 'bignumber.js';
import { SWAPS_CHAINID_DEFAULT_TOKEN_MAP } from 'constant';
import { ETH_TO_WEI } from 'constant/configs';

export const weiToEth = (value, fixed = 2) => {
  return new BigNumber(value).dividedBy(ETH_TO_WEI).toNumber().toFixed(fixed);
};

export const ethToWei = value => {
  return new BigNumber(value).multipliedBy(ETH_TO_WEI).toNumber();
};

export const amountConversion = (amount, perValuePrice, fixed = 18) => {
  const val = new BigNumber(amount).multipliedBy(perValuePrice).toNumber();
  return weiToEth(val, fixed);
};

export const amountConversionEthToWei = (amount, perValuePrice, fixed = 18) => {
  const val = new BigNumber(amount).multipliedBy(perValuePrice).toNumber();
  return ethToWei(val);
};

/**
 * Prefixes a hex string with '0x' or '-0x' and returns it. Idempotent.
 *
 * @param {string} str - The string to prefix.
 * @returns {string} The prefixed string.
 */
export const addHexPrefix = str => {
  if (typeof str !== 'string' || str.match(/^-?0x/u)) {
    return str;
  }

  if (str.match(/^-?0X/u)) {
    return str.replace('0X', '0x');
  }

  if (str.startsWith('-')) {
    return str.replace('-', '-0x');
  }

  return `0x${str}`;
};

/**
 * Given and object where all values are strings, returns the same object with all values
 * now prefixed with '0x'
 */
export function addHexPrefixToObjectValues(obj) {
  return Object.keys(obj).reduce((newObj, key) => {
    return { ...newObj, [key]: addHexPrefix(obj[key]) };
  }, {});
}

export function constructTxParams({
  sendToken,
  data,
  to,
  amount,
  from,
  gas,
  gasPrice,
}: {
  sendToken?: any;
  data?: string;
  from?: string;
  amount?: string;
  gas?: string;
  gasPrice?: string;
  to?: string;
}) {
  const txParams: {
    data?: string;
    from?: string;
    value?: string;
    gas?: string;
    gasPrice?: string;
    to?: string;
  } = {
    data,
    from,
    value: '0',
    gas,
    gasPrice,
  };

  if (!sendToken) {
    txParams.value = amount;
    txParams.to = to;
  }
  return addHexPrefixToObjectValues(txParams);
}

export function isSwapsDefaultTokenAddress(address, chainId) {
  if (!address || !chainId) {
    return false;
  }

  return address === SWAPS_CHAINID_DEFAULT_TOKEN_MAP[chainId]?.address;
}

export const processArray = async array => {
  let results: any[] = [];
  return array.reduce(function (p, item) {
    return p.then(function () {
      return item.then(function (data) {
        results.push(data);
        return results;
      });
    });
  }, Promise.resolve());
};

export const isNumeric = val => !isNaN(val);
