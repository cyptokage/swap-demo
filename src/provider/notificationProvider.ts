import { store } from 'react-notifications-component';

const addMessage = ({ title, message, type }) => {
  store.addNotification({
    title,
    message,
    type,
    insert: 'top',
    container: 'top-right',
    animationIn: ['animate__animated', 'animate__fadeIn'],
    animationOut: ['animate__animated', 'animate__fadeOut'],
    dismiss: {
      duration: 5000,
      onScreen: true,
      pauseOnHover: true,
    },
  });
};

export const addSuccess = (title, message) => {
  addMessage({ title, message, type: 'success' });
};

export const addError = message => {
  addMessage({ title: 'Error', message, type: 'danger' });
};
