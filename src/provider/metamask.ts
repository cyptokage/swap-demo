import detectEthereumProvider from '@metamask/detect-provider';
import { homeActions } from 'app/pages/HomePage/reducer';
import { store } from 'store/configureStore';
import Web3 from 'web3';

declare global {
  interface Window {
    web3: Web3;
  }
}

export interface ProviderRpcError extends Error {
  message: string;
  code?: number;
  data?: unknown;
}

export interface ProviderMessage {
  type: string;
  data: any;
}

export const MetaMaskSingletonObject = (function () {
  let instance;

  async function init() {
    const provider: any = await detectEthereumProvider();
    if (!provider) throw new Error('Metamask is not installed');
    provider.on('connect', (data: { chainId: string }) => {
      store.dispatch(homeActions.startMetaMaskSuccess(data.chainId));
    });

    provider.on('disconnect', (data: ProviderRpcError) => {
      store.dispatch(homeActions.startMetaMaskError(data.message));
    });
    provider.on('chainChanged', (data: string) => {
      store.dispatch(homeActions.startMetaMaskchainChanged(data));
    });
    provider.on('accountsChanged', (data: string[]) => {
      store.dispatch(homeActions.startMetaMaskAccountsChanged(data));
    });
    // provider.on('message', (data: ProviderMessage) => {
    //   store.dispatch(homeActions.startMetaMaskMessage(data));
    // });
    return provider;
  }

  return {
    getInstance: async function () {
      if (!instance) {
        instance = await init();
      }
      return instance;
    },
  };
})();

export const loadWeb3 = instance => {
  if (instance) {
    window.web3 = new Web3(instance);
    instance.enable();
  }
};
