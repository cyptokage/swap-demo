import {
  PROVIDER,
  ROUTERV2_CONTRACT_ADDRESS,
  ROUTERV2_CONTRACT_INTERFACE,
} from 'constant';
import ERC20ABI from 'human-standard-token-abi';

export const SwapProvider = (function () {
  let instance;
  const contractObj = {};

  async function init(chainId) {
    if (!window.web3) throw new Error('Web3 is not configred');
    contractObj[chainId] = {};
    const UNI_SWAP = await new window.web3.eth.Contract(
      //@ts-ignore
      ROUTERV2_CONTRACT_INTERFACE[PROVIDER.UNI_SWAP],
      ROUTERV2_CONTRACT_ADDRESS[chainId][PROVIDER.UNI_SWAP],
    );
    const SUSHI_SWAP = await new window.web3.eth.Contract(
      // @ts-ignore
      ROUTERV2_CONTRACT_INTERFACE[PROVIDER.SUSHI_SWAP],
      ROUTERV2_CONTRACT_ADDRESS[chainId][PROVIDER.SUSHI_SWAP],
    );

    contractObj[chainId] = {
      UNI_SWAP,
      SUSHI_SWAP,
    };
  }

  return {
    initialize: async function (chainId) {
      if (!instance) {
        await init(chainId);
      }
    },
    getContract: function (chainId, provider, type) {
      if (
        !(
          contractObj[chainId] &&
          contractObj[chainId][provider] &&
          contractObj[chainId][provider][type]
        )
      )
        throw new Error('Contract not found');
      return contractObj[chainId][provider][type];
    },
    getRouterV2Contract: function (chainId) {
      return contractObj[chainId];
    },
    getQuote: async function (chainId, { fromToken, toToken }) {
      const router = this.getRouterV2Contract(chainId);
      const pairPrice = await reduceToObject(
        router,
        fetchPairData(router, fromToken, toToken),
        Promise.resolve({}),
      );
      return pairPrice;
    },
    getToken: async function (tokenAddress) {
      const token = new window.web3.eth.Contract(ERC20ABI, tokenAddress);
      const name = await token.methods.name().call();
      const symbol = await token.methods.symbol().call();
      const decimals = await token.methods.decimals().call();
      return {
        name,
        symbol,
        decimals,
      };
    },
    executeSwap: async function (
      { swapProvider, chainId },
      { amountIn, path, to, deadline, amountOutMin },
      account,
    ) {
      const router = this.getRouterV2Contract(chainId);
      return router[swapProvider].methods
        .swapExactTokensForTokens(amountIn, amountOutMin, path, to, deadline)
        .send({ from: account });
    },
    allowance: async function ({
      fromAddress,
      toAddress,
      fromAmount,
      fromTokenAddress,
    }) {
      const token = new window.web3.eth.Contract(ERC20ABI, fromTokenAddress);
      return token.methods
        .approve(toAddress, fromAmount)
        .send({ from: fromAddress });
    },
  };
})();
export const reduceToObject = async (objToReduce, fn, initialValue) =>
  Object.keys(objToReduce).reduce(fn, initialValue);

const fetchPairData = (factoryContract, fromToken, toToken) => async (
  promisifiedAcc,
  item,
) => {
  const acc = await promisifiedAcc;
  try {
    acc[item] = await factoryContract[item].methods
      .getAmountsOut(1, [fromToken, toToken])
      .call();
  } catch (err) {}
  return acc;
};
