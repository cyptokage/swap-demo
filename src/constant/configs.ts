import IUniswapV2Factory from '@uniswap/v2-core/build/IUniswapV2Factory.json';
import IUniswapV2Router02 from '@uniswap/v2-periphery/build/IUniswapV2Router02.json';
import IUniswapV2Pair from '@uniswap/v2-core/build/IUniswapV2Pair.json';
import { SUSHIV2FACTORY, SUSHIV2ROUTER } from './abi';

export const WETH = 'WETH';
export const UNI = 'UNISWAP';
export const SUSHI = 'SUSHISWAP';
export const UNI_SWAP = 'UNI_SWAP';
export const SUSHI_SWAP = 'SUSHI_SWAP';

const MAINNET_CHAIN_ID = '0x1';
const ROPSTEN_CHAIN_ID = '0x3';
const RINKEBY_CHAIN_ID = '0x4';
const GOERLI_CHAIN_ID = '0x5';
const KOVAN_CHAIN_ID = '0x2a';

export const CHAIN_ID_TO_ETHER_SCAN = {
  [MAINNET_CHAIN_ID]: 'https://etherscan.io',
  [ROPSTEN_CHAIN_ID]: 'https://ropsten.etherscan.io',
  [RINKEBY_CHAIN_ID]: 'https://kovan.etherscan.io',
  [GOERLI_CHAIN_ID]: 'https://goerli.etherscan.io',
  [KOVAN_CHAIN_ID]: 'https://kovan.etherscan.io',
};
export const PREFIX_TX = 'tx';

export const TOKEN_MAP = {
  [WETH]: '0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2',
  [UNI]: '0x1f9840a85d5aF5bf1D1762F925BDADdC4201F984',
};
export const REVERSE_TOKEN_MAP = Object.keys(TOKEN_MAP).reduce((acc, curr) => {
  acc[TOKEN_MAP[curr]] = curr;
  return acc;
}, {});

export const ETH_TO_WEI = 1000000000000000000;

export const COLLAPSE_SCREEN_MAP = {
  SWAP: 1,
  QUOTES: 2,
};

export const Factory = 'Factory';
export const Router02 = 'Router02';
export const PairERC20 = 'PairERC20';

export const TYPE_CONTRACT = {
  Factory,
  Router02,
  PairERC20,
};

export const PROVIDER = {
  UNI_SWAP,
  SUSHI_SWAP,
};

export const PROVIDER_TO_NAME = {
  [PROVIDER.UNI_SWAP]: UNI,
  [PROVIDER.SUSHI_SWAP]: SUSHI,
};

export const FACTORY_CONTRACT_INTERFACE = {
  [PROVIDER.UNI_SWAP]: IUniswapV2Factory.abi,
  [PROVIDER.SUSHI_SWAP]: SUSHIV2FACTORY,
};

export const ROUTERV2_CONTRACT_INTERFACE = {
  [PROVIDER.UNI_SWAP]: IUniswapV2Router02.abi,
  [PROVIDER.SUSHI_SWAP]: SUSHIV2ROUTER,
};

export const PAIR_CONTRACT_INTERFACE = {
  [PROVIDER.UNI_SWAP]: IUniswapV2Pair.abi,
  [PROVIDER.SUSHI_SWAP]: IUniswapV2Pair.abi,
};

export const MAINNET_FACTORY_CONTRACT_ADDRESSES = {
  [PROVIDER.UNI_SWAP]: '0x5C69bEe701ef814a2B6a3EDD4B1652CB9cc5aA6f',
  [PROVIDER.SUSHI_SWAP]: '0xC0AEe478e3658e2610c5F7A4A2E1777cE9e4f2Ac',
};

export const TESTNET_FACTORY_CONTRACT_ADDRESSES = {
  [PROVIDER.UNI_SWAP]: '0x5C69bEe701ef814a2B6a3EDD4B1652CB9cc5aA6f',
  [PROVIDER.SUSHI_SWAP]: '0xC0AEe478e3658e2610c5F7A4A2E1777cE9e4f2Ac',
};

export const TESTNET_ROUTERV2_CONTRACT_ADDRESSES = {
  [PROVIDER.UNI_SWAP]: '0x7a250d5630b4cf539739df2c5dacb4c659f2488d',
  [PROVIDER.SUSHI_SWAP]: '0x1b02dA8Cb0d097eB8D57A175b88c7D8b47997506',
};

export const MAINNET_ROUTERV2_CONTRACT_ADDRESSES = {
  [PROVIDER.UNI_SWAP]: '0x7a250d5630b4cf539739df2c5dacb4c659f2488d',
  [PROVIDER.SUSHI_SWAP]: '0xd9e1cE17f2641f24aE83637ab66a2cca9C378B9F',
};

export const FACTORY_CONTRACT_ADDRESS = {
  [MAINNET_CHAIN_ID]: MAINNET_FACTORY_CONTRACT_ADDRESSES,
  [ROPSTEN_CHAIN_ID]: TESTNET_FACTORY_CONTRACT_ADDRESSES,
  [RINKEBY_CHAIN_ID]: TESTNET_FACTORY_CONTRACT_ADDRESSES,
  [GOERLI_CHAIN_ID]: TESTNET_FACTORY_CONTRACT_ADDRESSES,
  [KOVAN_CHAIN_ID]: TESTNET_FACTORY_CONTRACT_ADDRESSES,
};

export const ROUTERV2_CONTRACT_ADDRESS = {
  [MAINNET_CHAIN_ID]: MAINNET_ROUTERV2_CONTRACT_ADDRESSES,
  [ROPSTEN_CHAIN_ID]: TESTNET_ROUTERV2_CONTRACT_ADDRESSES,
  [RINKEBY_CHAIN_ID]: TESTNET_ROUTERV2_CONTRACT_ADDRESSES,
  [GOERLI_CHAIN_ID]: TESTNET_ROUTERV2_CONTRACT_ADDRESSES,
  [KOVAN_CHAIN_ID]: TESTNET_ROUTERV2_CONTRACT_ADDRESSES,
};

export const DELAY_LOGIN = 10000;
export const DELAY_CLOSE_TX_SUCCESS_COLLAPSE = 10000;

export const SLIPPAGE_VALUE = [0, 0.025, 0.05, 0.1];
