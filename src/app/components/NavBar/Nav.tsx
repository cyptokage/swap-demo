import React from 'react';
import styled from 'styled-components/macro';
import { Button, Form, Navbar, Nav as NavBS, Badge } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { createSelector } from 'reselect';
import makeHomeSelector from 'app/pages/HomePage/selectors';
import { homeActions, MetamaskInterface } from 'app/pages/HomePage/reducer';
import { weiToEth } from 'utils/utils';
import { NETWORK_TO_NAME_MAP } from 'constant';
import { selectThemeKey } from 'styles/theme/slice/selectors';
import { themes } from 'styles/theme/themes';
import PulseLoaders from 'react-spinners/PulseLoader';

const selector = createSelector(
  makeHomeSelector,
  selector => selector.metamask,
);

export function Nav() {
  const {
    isConnected,
    accounts,
    chainId,
    balance,
    isLoading,
  }: MetamaskInterface = useSelector(selector);
  const theme = useSelector(selectThemeKey);
  const selectedTheme = themes[theme];
  const dispatch = useDispatch();
  return (
    <Navbar bg="light" variant="light" className="w-100">
      <Navbar.Brand href="#home">Swap</Navbar.Brand>
      <NavBS className="mr-auto" />
      <Form inline>
        <Badge>{NETWORK_TO_NAME_MAP[chainId]}</Badge>
        <Badge>{weiToEth(balance)} ETH</Badge>
        <Button
          variant="outline-primary"
          disabled={isConnected}
          onClick={() => dispatch(homeActions.initialize())}
        >
          {!isLoading ? (
            isConnected ? (
              <ElipsisText>{accounts[0]}</ElipsisText>
            ) : (
              'Connect to MetaMask'
            )
          ) : (
            <PulseLoaders
              color={selectedTheme?.loaderColor}
              loading={isLoading}
              size={18}
            />
          )}
        </Button>
      </Form>
    </Navbar>
  );
}

// const Wrapper = styled.nav`
//   display: flex;
//   margin-right: -1rem;
// `;

// const NoUnderline = styled.a`
//   color: ${p => p.theme.primary};
//   cursor: pointer;
//   text-decoration: none;
//   display: flex;
//   padding: 0.25rem 1rem;
//   font-size: 0.875rem;
//   font-weight: 500;
//   align-items: center;

//   &:hover {
//     opacity: 0.8;
//     text-decoration: none;
//   }

//   &:active {
//     opacity: 0.4;
//   }

//   .icon {
//     margin-right: 0.25rem;
//   }
// `;

// const Item = styled.a`
//   color: ${p => p.theme.primary};
//   cursor: pointer;
//   text-decoration: none;
//   display: flex;
//   padding: 0.25rem 1rem;
//   font-size: 0.875rem;
//   font-weight: 500;
//   align-items: center;

//   &:hover {
//     opacity: 0.8;
//   }

//   &:active {
//     opacity: 0.4;
//   }

//   .icon {
//     margin-right: 0.25rem;
//   }
// `;

const ElipsisText = styled.div`
  white-space: nowrap;
  width: 100px;
  overflow: hidden;
  text-overflow: ellipsis;
`;
