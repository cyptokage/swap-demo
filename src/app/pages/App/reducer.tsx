import { createSlice } from 'utils/@reduxjs/toolkit';

export interface AppState {}

export const initialState: AppState = {};

const slice = createSlice({
  name: 'app',
  initialState,
  reducers: {
    initializeMetamask: state => {},
  },
});

export const { actions: appActions, reducer, name: appName } = slice;
