import { MetaMaskSingletonObject } from 'provider/metamask';
import { put, call, takeLatest } from 'redux-saga/effects';
import { homeActions } from '../HomePage/reducer';
import { appActions } from './reducer';

function* initMetamask() {
  try {
    yield put(homeActions.resetMetaMask());
    yield call(MetaMaskSingletonObject.getInstance);
  } catch (err) {
    yield put(homeActions.startMetaMaskError(err.message));
  }
}

export default function* mySaga() {
  yield takeLatest(appActions.initializeMetamask.type, initMetamask);
}
