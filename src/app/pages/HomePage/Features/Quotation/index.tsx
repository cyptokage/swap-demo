import { PROVIDER_TO_NAME } from 'constant';
import React from 'react';
import { Card, Row, Col } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import { pairDataSelector } from '../../selectors';

const Quotation = ({ data, onClickHandler }) => {
  const { priceFromToToToken, deadline, swapProvider, minAmtReceived } = data;
  const { fromToken, toToken } = useSelector(pairDataSelector);
  const { t } = useTranslation();
  return (
    <Card className="my-2">
      <Card.Body>
        <Row>
          <input
            type="radio"
            id={swapProvider}
            name={`${fromToken.name}-${toToken.name}`}
            onClick={onClickHandler}
          />
          <Col xs="6">
            <dt>
              {t('homePage.qoute.toTokenperFromToken', {
                fromToken: fromToken.symbol,
              })}
            </dt>
            <dd>
              {priceFromToToToken} {toToken.symbol}
            </dd>
          </Col>
          <Col xs="6">
            <dt>{t('homePage.qoute.minimumReceived')}</dt>
            <dd>
              {minAmtReceived} {toToken.symbol}
            </dd>
          </Col>
          <Col xs="6">
            <dt>{t('homePage.qoute.deadline')}</dt>
            <dd>{new Date(deadline * 1000).toString()}</dd>
          </Col>
        </Row>
      </Card.Body>
      <Card.Footer>
        {t('homePage.qoute.poweredBy', {
          swapProvider: PROVIDER_TO_NAME[swapProvider],
        })}
      </Card.Footer>
    </Card>
  );
};

export default Quotation;
