import { CHAIN_ID_TO_ETHER_SCAN, PREFIX_TX } from 'constant';
import React from 'react';
import { Card } from 'react-bootstrap';
import { useSelector } from 'react-redux';
import { metamaskSelector, txSuccessseSelector } from '../../selectors';

const TransactionSuccessCard = () => {
  const { data: txHash } = useSelector(txSuccessseSelector);
  const { chainId } = useSelector(metamaskSelector);

  return (
    <Card bg="success" className="mx-2">
      <Card.Body className="d-flex justify-content-center p-2">
        <a
          href={`${CHAIN_ID_TO_ETHER_SCAN[chainId]}/${PREFIX_TX}/${txHash}`}
          target="_blank"
          rel="noopener noreferrer"
        >
          Tx hash: {txHash}
        </a>
      </Card.Body>
    </Card>
  );
};

export default TransactionSuccessCard;
