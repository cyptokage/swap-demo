import { SLIPPAGE_VALUE } from 'constant';
import React from 'react';
import { Pagination } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { homeActions } from '../../reducer';
import { slippageSelector } from '../../selectors';

const Slippage = () => {
  const slippage = useSelector(slippageSelector);
  const dispatch = useDispatch();
  const { t } = useTranslation();
  return (
    <div className="d-flex">
      <span className="my-1">
        <h5>{t('homePage.slippage.title')}</h5>
      </span>{' '}
      <span>
        <Pagination size="sm" className="ml-2">
          {SLIPPAGE_VALUE.map(item => (
            <Pagination.Item
              active={item === slippage}
              onClick={() => dispatch(homeActions.updateSlippage(item))}
            >
              {item * 100}%
            </Pagination.Item>
          ))}
        </Pagination>
      </span>
    </div>
  );
};
export default Slippage;
