import React from 'react';
import { Modal, Button } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { metamaskSelector } from '../../selectors';

const MetaMaskErrorModal = () => {
  const { isError } = useSelector(metamaskSelector);
  const dispatch = useDispatch();
  const { t } = useTranslation();
  return (
    <Modal show={!!isError} centered>
      <Modal.Header>
        <Modal.Title>{t('homePage.metamaskError.heading')}</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <p>{isError}</p>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={() => dispatch(() => window.location.reload())}>
          {t('homePage.metamaskError.closeAndTryAgain')}
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default MetaMaskErrorModal;
