import BigNumber from 'bignumber.js';
import {
  COLLAPSE_SCREEN_MAP,
  DELAY_CLOSE_TX_SUCCESS_COLLAPSE,
  DELAY_LOGIN,
  ETH_TO_WEI,
  ROUTERV2_CONTRACT_ADDRESS,
} from 'constant/configs';
import { loadWeb3, MetaMaskSingletonObject } from 'provider/metamask';
import { addError } from 'provider/notificationProvider';
import { SwapProvider } from 'provider/swapProvider';
import {
  call,
  delay,
  fork,
  put,
  race,
  select,
  take,
  takeEvery,
  takeLatest,
} from 'redux-saga/effects';
import { homeActions, HomeState } from './reducer';
import { homeSelector } from './selectors';
import orderBy from 'lodash/orderBy';
import { amountConversion } from 'utils/utils';

function* getBalance(accounts) {
  const ethereum = yield MetaMaskSingletonObject.getInstance();
  const balance = yield call(ethereum.request, {
    method: 'eth_getBalance',
    params: [accounts[0], 'latest'],
  });
  return parseInt(balance, 16);
}

function* startMetaMaskLoading() {
  yield put(homeActions.startMetaMaskLoading());
  try {
    const ethereum = yield MetaMaskSingletonObject.getInstance();
    const accounts = yield call(ethereum.request, {
      method: 'eth_requestAccounts',
    });

    const chainId = yield call(ethereum.request, {
      method: 'eth_chainId',
    });

    const balance = yield call(getBalance, accounts);

    yield put(
      homeActions.startMetaMaskConnected({
        isConnected: true,
        accounts,
        balance,
        chainId: chainId,
      }),
    );
  } catch (err) {
    addError(err.message);
    yield put(homeActions.startMetaMaskError(err.message));
  }
}

function* updateBalance(action) {
  try {
    const balance = yield call(getBalance, action.payload);
    yield put(homeActions.startMetaMaskBalanceChanged(balance));
  } catch (err) {
    addError(err.message);
  }
}

function* fetchPairData(action) {
  try {
    const {
      metamask: { chainId },
      slippage,
    }: HomeState = yield select(homeSelector);
    const { fromToken, toToken, fromAmount } = action.payload;
    const pairPriceData: any = yield SwapProvider.getQuote(
      chainId,
      action.payload,
    );

    const updatedPriceObj = Object.keys(pairPriceData).map(item => {
      const arr = pairPriceData[item];
      const priceFromToToToken = new BigNumber(arr[1])
        .toNumber()
        .toLocaleString('fullwide', { useGrouping: false });
      const minAmtReceived = new BigNumber(1 - slippage)
        .multipliedBy(amountConversion(fromAmount, priceFromToToToken, 6))
        .toNumber()
        .toLocaleString('fullwide', { useGrouping: false });

      return {
        priceFromToToToken,
        priceToToFromToken: new BigNumber(arr[0])
          .dividedBy(priceFromToToToken)
          .toNumber()
          .toLocaleString('fullwide', { useGrouping: false }),
        fromToken,
        toToken,
        deadline: Date.now() / 1000 + 60,
        fromAmount,
        swapProvider: item,
        minAmtReceived,
      };
    });
    if (!updatedPriceObj.length) throw new Error('No Pair Data Found');
    yield put(
      homeActions.startFetchPairDataSuccess(
        orderBy(updatedPriceObj, ['minAmtReceived'], ['asc']),
      ),
    );
    yield put(homeActions.changeCollapseScreen(COLLAPSE_SCREEN_MAP.QUOTES));
  } catch (err) {
    addError(err.message);
    yield put(homeActions.startFetchPairDataFailure(err.message));
  }
}

function* getToken(action) {
  const {
    payload: { type, data },
  } = action;
  const defaultObj = {
    isLoading: false,
    isError: '',
    name: '',
    symbol: '',
    decimals: 0,
  };
  try {
    if (type === 'from') {
      yield put(
        homeActions.updateTokenFrom({
          ...defaultObj,
          isLoading: true,
        }),
      );
      const { name, symbol, decimals } = yield SwapProvider.getToken(data);
      yield put(
        homeActions.updateTokenFrom({
          ...defaultObj,
          name,
          symbol,
          decimals,
        }),
      );
      return { name, symbol, decimals };
    }
    yield put(
      homeActions.updateTokenTo({
        ...defaultObj,
        isLoading: true,
      }),
    );
    const { name, symbol, decimals } = yield SwapProvider.getToken(data);
    yield put(
      homeActions.updateTokenTo({
        ...defaultObj,
        name,
        symbol,
        decimals,
      }),
    );
    return { name, symbol, decimals };
  } catch (err) {
    addError(err.message);
    if (type === 'from') {
      yield put(
        homeActions.updateTokenFrom({
          ...defaultObj,
          isError: err.message,
        }),
      );
    } else {
      yield put(
        homeActions.updateTokenFrom({
          ...defaultObj,
          isError: err.message,
        }),
      );
    }
  }
}

function* startApprove(action) {
  yield put(homeActions.closeTxSuccess());
  try {
    const {
      metamask: { chainId, accounts },
    }: HomeState = yield select(homeSelector);
    const { fromAmount, fromToken, swapProvider, idx } = action.payload;
    const approve = yield SwapProvider.allowance({
      fromAddress: accounts[0],
      toAddress: ROUTERV2_CONTRACT_ADDRESS[chainId][swapProvider],
      fromAmount: new BigNumber(fromAmount)
        .toNumber()
        .toLocaleString('fullwide', { useGrouping: false }),
      fromTokenAddress: fromToken,
    });
    yield put(homeActions.startApproveSuccess({ approve, idx }));
    yield put(homeActions.openTxSuccess(approve.transactionHash));
    // addSuccess('Tx Success', approve.transactionHash);
  } catch (err) {
    addError(err.message);
    yield put(homeActions.startApproveFailure(err.message));
  }
}

function* startSwaping(action) {
  yield put(homeActions.closeTxSuccess());
  try {
    const {
      metamask: { chainId, accounts },
      slippage,
    }: HomeState = yield select(homeSelector);
    const {
      priceFromToToToken,
      fromAmount,
      fromToken,
      toToken,
      swapProvider,
    } = action.payload;
    const query = {
      amountIn: new BigNumber(fromAmount)
        .toNumber()
        .toLocaleString('fullwide', { useGrouping: false }),
      path: [fromToken, toToken],
      to: accounts[0],
      deadline: Math.floor(Date.now() / 1000) + 60 * 20,
      amountOutMin: new BigNumber(fromAmount)
        .multipliedBy(priceFromToToToken)
        .multipliedBy(1 - slippage)
        .toNumber()
        .toLocaleString('fullwide', { useGrouping: false }),
    };
    const swap = yield SwapProvider.executeSwap(
      { swapProvider, chainId },
      query,
      accounts[0],
    );
    yield put(homeActions.resetApproveState());
    yield put(homeActions.startSwapSuccess(swap));
    yield put(homeActions.changeCollapseScreen(COLLAPSE_SCREEN_MAP.SWAP));
    yield put(homeActions.openTxSuccess(swap.transactionHash));
  } catch (err) {
    addError(err.message);
    yield put(homeActions.startSwapFailure(err.message));
  }
}

function* startWeb3() {
  yield put(homeActions.changeWeb3Status({ isLoading: true, isError: '' }));
  try {
    const ethereum = yield MetaMaskSingletonObject.getInstance();
    yield call(loadWeb3, ethereum);
    yield put(homeActions.changeWeb3Status({ isLoading: false, isError: '' }));
  } catch (err) {
    yield put(
      homeActions.changeWeb3Status({ isLoading: false, isError: err.message }),
    );
  }
}

function* startSwapProvider() {
  yield put(homeActions.changeSwapProvider({ isLoading: true, isError: '' }));
  try {
    const {
      metamask: { chainId },
    }: HomeState = yield select(homeSelector);
    yield call(SwapProvider.initialize, chainId);
    yield put(
      homeActions.changeSwapProvider({ isLoading: false, isError: '' }),
    );
  } catch (err) {
    yield put(
      homeActions.changeSwapProvider({
        isLoading: false,
        isError: err.message,
      }),
    );
  }
}

function* raceConnectionMetamask() {
  try {
    yield take(homeActions.startMetaMaskLoading.type);
    const { failure } = yield race({
      success: take(homeActions.startMetaMaskConnected.type),
      failure: delay(DELAY_LOGIN),
    });
    if (failure) {
      throw new Error('Unable to connect with metamask. Please try again');
    }
  } catch (err) {
    addError(err.message);
    yield put(homeActions.startMetaMaskError(err.message));
  }
}

function* handleChangeChain() {
  const {
    metamask: { accounts },
  }: HomeState = yield select(homeSelector);
  yield call(updateBalance, { payload: accounts });
  yield call(startSwapProvider);
}

function* initalize() {
  yield call(startMetaMaskLoading);
  yield call(startWeb3);
  yield call(startSwapProvider);
}

function* closeTxSuccess() {
  yield delay(DELAY_CLOSE_TX_SUCCESS_COLLAPSE);
  yield put(homeActions.closeTxSuccess());
}

export default function* mySaga() {
  yield takeLatest(
    homeActions.startMetaMaskAccountsChanged.type,
    updateBalance,
  );
  yield takeLatest(
    homeActions.startMetaMaskchainChanged.type,
    handleChangeChain,
  );
  yield takeLatest(homeActions.startFetchPairDataLoading.type, fetchPairData);
  yield takeLatest(homeActions.initialize.type, initalize);
  yield takeLatest(homeActions.startApproveLoading.type, startApprove);
  yield takeLatest(homeActions.startSwapLoading.type, startSwaping);
  yield takeLatest(homeActions.openTxSuccess.type, closeTxSuccess);
  yield takeEvery(homeActions.getTokenData.type, getToken);
  yield fork(raceConnectionMetamask);
}
