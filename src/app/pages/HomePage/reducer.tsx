import { PayloadAction } from '@reduxjs/toolkit';
import { COLLAPSE_SCREEN_MAP, SLIPPAGE_VALUE } from 'constant/configs';
import { ProviderMessage } from 'provider/metamask';
import { createSlice } from 'utils/@reduxjs/toolkit';

export interface MetamaskInterface extends DEFAULT_LOADING_ERROR {
  isConnected: boolean;
  accounts: string[];
  chainId: string;
  message: any & ProviderMessage;
  balance: number;
}

export interface TxSuccess {
  isOpen: boolean;
  data: string;
}

export interface DEFAULT_LOADING_ERROR {
  isLoading: boolean;
  isError: string;
}

export interface DEFAULT_STATE extends DEFAULT_LOADING_ERROR {
  data: any;
}

export interface ERC20_TOKEN extends DEFAULT_LOADING_ERROR {
  name: string;
  symbol: string;
  decimals: number;
}

export interface POOL_PAIR_STATE extends DEFAULT_STATE {
  fromToken: ERC20_TOKEN;
  toToken: ERC20_TOKEN;
}

export interface HomeState {
  metamask: MetamaskInterface;
  web3Status: DEFAULT_LOADING_ERROR;
  swapProvider: DEFAULT_LOADING_ERROR;
  pairData: POOL_PAIR_STATE;
  tradeReady: DEFAULT_STATE;
  approve: DEFAULT_STATE;
  swap: DEFAULT_STATE;
  collapsed: number;
  txSuccess: TxSuccess;
  slippage: number;
}

export const initialState: HomeState = {
  metamask: {
    isConnected: false,
    accounts: [],
    chainId: '',
    message: {},
    balance: 0,
    isLoading: false,
    isError: '',
  },
  pairData: {
    isLoading: false,
    isError: '',
    data: [],
    fromToken: {
      name: '',
      symbol: '',
      decimals: 0,
      isError: '',
      isLoading: false,
    },
    toToken: {
      name: '',
      symbol: '',
      decimals: 0,
      isError: '',
      isLoading: false,
    },
  },
  tradeReady: {
    isLoading: false,
    isError: '',
    data: {},
  },
  approve: {
    isLoading: false,
    isError: '',
    data: {},
  },
  swap: {
    isLoading: false,
    isError: '',
    data: {},
  },
  collapsed: COLLAPSE_SCREEN_MAP.SWAP,
  web3Status: {
    isLoading: false,
    isError: '',
  },
  swapProvider: {
    isLoading: false,
    isError: '',
  },
  txSuccess: {
    isOpen: false,
    data: '',
  },
  slippage: SLIPPAGE_VALUE[0],
};

const slice = createSlice({
  name: 'home',
  initialState,
  reducers: {
    startMetaMaskLoading: state => {
      state.metamask.isLoading = true;
      state.metamask.isConnected = false;
      state.metamask.isError = '';
    },
    startMetaMaskSuccess: (state, action: PayloadAction<string>) => {
      state.metamask.chainId = action.payload;
    },
    startMetaMaskConnected: (
      state,
      action: PayloadAction<{
        isConnected: boolean;
        accounts: string[];
        balance: number;
        chainId: string;
      }>,
    ) => {
      state.metamask.isLoading = false;
      state.metamask.isConnected = action.payload.isConnected;
      state.metamask.accounts = action.payload.accounts;
      state.metamask.balance = action.payload.balance;
      state.metamask.chainId = action.payload.chainId;
    },
    startMetaMaskAccountsChanged: (state, action: PayloadAction<string[]>) => {
      state.metamask.accounts = action.payload;
    },
    startMetaMaskBalanceChanged: (state, action: PayloadAction<number>) => {
      state.metamask.balance = action.payload;
    },
    startMetaMaskchainChanged: (state, action: PayloadAction<string>) => {
      state.metamask.chainId = action.payload;
    },
    startMetaMaskMessage: (state, action: PayloadAction<ProviderMessage>) => {
      state.metamask.message = action.payload;
    },
    startMetaMaskError: (state, action: PayloadAction<string>) => {
      state.metamask.isLoading = false;
      state.metamask.isConnected = false;
      state.metamask.isError = action.payload;
    },
    resetMetaMask: state => {
      state.metamask.isConnected = false;
      state.metamask.isLoading = false;
      state.metamask.isError = '';
      state.metamask.accounts = [];
      state.metamask.chainId = '';
      state.metamask.message = {};
    },
    startFetchPairDataLoading: (state, action) => {
      state.pairData.isLoading = true;
      state.pairData.data = [];
      state.pairData.isError = '';
    },
    startFetchPairDataSuccess: (state, action) => {
      state.pairData.isLoading = false;
      state.pairData.data = action.payload;
      state.pairData.isError = '';
    },
    startFetchPairDataFailure: (state, action: PayloadAction<string>) => {
      state.pairData.isLoading = false;
      state.pairData.data = [];
      state.pairData.isError = action.payload;
    },
    updateTokenFrom: (state, action: PayloadAction<ERC20_TOKEN>) => {
      state.pairData.fromToken = action.payload;
    },
    updateTokenTo: (state, action: PayloadAction<ERC20_TOKEN>) => {
      state.pairData.toToken = action.payload;
    },
    startApproveLoading: (state, action) => {
      state.approve.isLoading = true;
      state.approve.data = {};
      state.approve.isError = '';
    },
    startApproveSuccess: (
      state,
      action: PayloadAction<{
        approve: any;
        idx: number;
      }>,
    ) => {
      state.approve.isLoading = false;
      state.approve.data = action.payload;
      state.approve.isError = '';
    },
    startApproveFailure: (state, action) => {
      state.approve.isLoading = false;
      state.approve.data = {};
      state.approve.isError = action.payload;
    },
    resetApproveState: state => {
      state.approve = {
        isLoading: false,
        isError: '',
        data: {},
      };
    },
    startSwapLoading: (state, action) => {
      state.swap.isLoading = true;
      state.swap.data = {};
      state.swap.isError = '';
    },
    startSwapSuccess: (state, action) => {
      state.swap.isLoading = false;
      state.swap.data = action.payload;
      state.swap.isError = '';
    },
    startSwapFailure: (state, action) => {
      state.swap.isLoading = false;
      state.swap.data = {};
      state.swap.isError = action.payload;
    },
    resetSwapState: state => {
      state.approve = {
        isLoading: false,
        isError: '',
        data: {},
      };
    },
    changeCollapseScreen: (state, action: PayloadAction<number>) => {
      state.collapsed = action.payload;
    },
    changeWeb3Status: (state, action: PayloadAction<DEFAULT_LOADING_ERROR>) => {
      state.web3Status = action.payload;
    },
    changeSwapProvider: (
      state,
      action: PayloadAction<DEFAULT_LOADING_ERROR>,
    ) => {
      state.swapProvider = action.payload;
    },
    initialize: () => {},
    openTxSuccess: (state, action: PayloadAction<string>) => {
      state.txSuccess.isOpen = true;
      state.txSuccess.data = action.payload;
    },
    closeTxSuccess: state => {
      state.txSuccess.isOpen = false;
      state.txSuccess.data = '';
    },
    getTokenData: (state, action) => {},
    updateSlippage: (state, action: PayloadAction<number>) => {
      state.slippage = action.payload;
    },
  },
});

export const { actions: homeActions, reducer, name } = slice;
