import React, { useState, useCallback, useEffect } from 'react';
// import { Helmet } from 'react-helmet-async';
import { NavBar } from 'app/components/NavBar';
import { PageWrapper } from 'app/components/PageWrapper';
import { homeActions, name, reducer } from './reducer';
import saga from './saga';
import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';
import PulseLoaders from 'react-spinners/PulseLoader';
import {
  Button,
  Card,
  Col,
  Row,
  Collapse,
  Form,
  FormControl,
  ButtonGroup,
} from 'react-bootstrap';
import { useForm } from 'react-hook-form';
import { COLLAPSE_SCREEN_MAP, ETH_TO_WEI } from 'constant/configs';
// import { ethToWeiString } from 'utils/utils';
import { useDispatch, useSelector } from 'react-redux';
import {
  pairDataSelector,
  collapseSelector,
  approveSelector,
  swapSelector,
  txSuccessseSelector,
  metamaskSelector,
} from './selectors';
import Quotation from './Features/Quotation';
import { MdSwapHoriz } from 'react-icons/md';
import { useTranslation } from 'react-i18next';
import isEmpty from 'lodash/isEmpty';
import { BigNumber } from 'bignumber.js';
import { selectThemeKey } from 'styles/theme/slice/selectors';
import { themes } from 'styles/theme/themes';
import TransactionSuccessCard from './Features/TransactionSuccessCard';
import Slippage from './Features/Slippage';
import useDebounce from 'app/components/useDebounce';

export function HomePage() {
  useInjectSaga({ key: name, saga });
  useInjectReducer({ key: name, reducer });
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const pairData = useSelector(pairDataSelector);
  const { fromToken: fromTokenDetails, toToken: toTokenDetails } = pairData;
  const collapsed = useSelector(collapseSelector);
  const approveState = useSelector(approveSelector);
  const swapState = useSelector(swapSelector);
  const theme = useSelector(selectThemeKey);
  const txSuccess = useSelector(txSuccessseSelector);
  const { accounts } = useSelector(metamaskSelector);
  const selectedTheme = themes[theme];
  const [readyToSwap, setReadyToSwap] = useState<any>({});
  const isApproveDataEmpty = isEmpty(approveState.data);
  const {
    register,
    handleSubmit,
    watch,
    getValues,
    formState,
    setValue,
  } = useForm({
    mode: 'onBlur',
    reValidateMode: 'onChange',
    defaultValues: {
      fromToken: '',
      toToken: '',
      fromAmount: '',
    },
  });
  const fromTokenRegister = register('fromToken', {
    required: true,
    minLength: 20,
  });

  const toTokenRegister = register('toToken', {
    required: true,
    minLength: 20,
  });

  const fromToken = watch('fromToken');
  const toToken = watch('toToken');
  const fromAmountValues = getValues('fromAmount');
  const fromTokenDebounce = useDebounce(fromToken, 500);
  const toTokenDebounce = useDebounce(toToken, 500);

  const onSubmit = ({ fromToken, toToken, fromAmount }) => {
    if (collapsed === COLLAPSE_SCREEN_MAP.QUOTES) {
      if (isApproveDataEmpty) {
        return dispatch(homeActions.startApproveLoading(readyToSwap));
      }
      return dispatch(homeActions.startSwapLoading(readyToSwap));
    }
    const isFormUpdated = fromToken && toToken && fromAmount;
    if (isFormUpdated) {
      dispatch(
        homeActions.startFetchPairDataLoading({
          fromToken,
          fromAmount: new BigNumber(fromAmount)
            .multipliedBy(ETH_TO_WEI)
            .toString(),
          toToken,
        }),
      );
    }
  };

  const GetQuotations = useCallback(
    () =>
      pairData.data.map((item, idx) => (
        <Quotation
          key={idx}
          data={{
            ...item,
            idx,
          }}
          onClickHandler={() => {
            if (readyToSwap.idx !== idx) {
              return setReadyToSwap({
                ...item,
                idx,
              });
            }
            return setReadyToSwap({});
          }}
        />
      )),
    [pairData.data, readyToSwap],
  );

  const flip = () => {
    setValue('fromToken', toToken);
    setValue('toToken', fromToken);
  };

  const getTokenData = useCallback(
    (type, data) => {
      dispatch(homeActions.getTokenData({ type, data }));
    },
    [dispatch],
  );

  useEffect(
    () => {
      if (fromTokenDebounce) {
        getTokenData('from', fromTokenDebounce);
      }
    },
    [fromTokenDebounce, getTokenData], // Only call effect if debounced search term changes
  );

  useEffect(
    () => {
      if (toTokenDebounce) {
        getTokenData('to', toTokenDebounce);
      }
    },
    [toTokenDebounce, getTokenData], // Only call effect if debounced search term changes
  );
  const isSymbolPresent = !!fromTokenDetails.symbol && !!toTokenDetails.symbol;
  const SwapForm = () => (
    <>
      <Row>
        <Col xs="12" md="5">
          <div className="my-2 text-center">
            From: {fromTokenDetails.name} ({fromTokenDetails.symbol || ' '})
          </div>
          <FormControl
            placeholder={t('homePage.fromToken')}
            aria-label="From Token Address"
            {...fromTokenRegister}
          />
        </Col>
        <Col xs="12" md="2">
          <div className="text-center">
            <Button
              variant="light"
              className="p-0 m-2"
              onClick={flip}
              disabled={!fromToken || !toToken}
            >
              <MdSwapHoriz size={24} />
            </Button>
          </div>
        </Col>
        <Col xs="12" md="5">
          <div className="my-2 text-center">
            To: {toTokenDetails.name} ({toTokenDetails.symbol || ' '})
          </div>
          <FormControl
            placeholder={t('homePage.toToken')}
            aria-label="To Token Address"
            {...toTokenRegister}
          />
        </Col>

        <Col xs="12" className="my-4">
          <FormControl
            placeholder={t('homePage.fromAmount')}
            aria-label="From"
            {...register('fromAmount', {
              required: true,
            })}
            type="number"
            min="0"
            step={new BigNumber(1).dividedBy(ETH_TO_WEI).toString()}
          />
        </Col>
        <Col xs="12">
          <Slippage />
        </Col>
      </Row>
    </>
  );
  const waitingText = () => {
    if (pairData.isLoading) {
      return t('homePage.analyzingQuotes');
    }
    if (approveState.isLoading) {
      return t('homePage.approving', {
        amount: getValues('fromAmount'),
        fromToken: fromTokenDetails.symbol,
        account: accounts[0],
      });
    }
    if (swapState.isLoading) {
      return t('homePage.swapping', {
        fromAmount: fromAmountValues,
        fromToken: fromTokenDetails.symbol,
        toToken: toTokenDetails.symbol,
        toAmount: new BigNumber(readyToSwap.priceFromToToToken)
          .multipliedBy(fromAmountValues)
          .toString(),
      });
    }
  };
  return (
    <>
      <NavBar />
      <PageWrapper>
        <Card>
          <Card.Body>
            <Form onSubmit={handleSubmit(onSubmit)}>
              <Collapse in={collapsed === COLLAPSE_SCREEN_MAP.SWAP}>
                <div id="swap-form">{SwapForm()}</div>
              </Collapse>
              <Collapse in={collapsed === COLLAPSE_SCREEN_MAP.QUOTES}>
                <div id="quotes-screen">
                  <Button
                    variant="secondary"
                    type="button"
                    className="my-2"
                    block
                    onClick={() => {
                      setReadyToSwap({});
                      dispatch(homeActions.resetApproveState());
                      dispatch(homeActions.resetSwapState());
                      dispatch(
                        homeActions.changeCollapseScreen(
                          COLLAPSE_SCREEN_MAP.SWAP,
                        ),
                      );
                    }}
                  >
                    {t('homePage.goBack')}
                  </Button>
                  {GetQuotations()}
                </div>
              </Collapse>
              <div className="text-center my-4">
                <Form.Text muted>{waitingText()}</Form.Text>
                <PulseLoaders
                  color={selectedTheme?.loaderColor}
                  loading={
                    pairData.isLoading ||
                    approveState.isLoading ||
                    swapState.isLoading
                  }
                  size={20}
                />
              </div>
              {collapsed === COLLAPSE_SCREEN_MAP.QUOTES ? (
                <ButtonGroup size="sm" className="w-100">
                  <Button
                    variant="primary"
                    type="submit"
                    disabled={
                      readyToSwap.idx === undefined || !isApproveDataEmpty
                    }
                  >
                    {t('homePage.ready')}
                  </Button>
                  <Button
                    variant="success"
                    type="submit"
                    disabled={
                      isApproveDataEmpty ||
                      approveState.data?.idx !== readyToSwap.idx
                    }
                  >
                    {t('homePage.swap')}
                  </Button>
                </ButtonGroup>
              ) : (
                <Button
                  type="submit"
                  block
                  disabled={
                    !formState.isDirty || !formState.isValid || !isSymbolPresent
                  }
                >
                  {t('homePage.submit')}
                </Button>
              )}
            </Form>
          </Card.Body>
          <Collapse in={txSuccess.isOpen} className="my-2">
            <div id="example-collapse-text">
              <TransactionSuccessCard />
            </div>
          </Collapse>
        </Card>
      </PageWrapper>
    </>
  );
}
