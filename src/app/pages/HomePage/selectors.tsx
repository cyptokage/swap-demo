import { COLLAPSE_SCREEN_MAP, SLIPPAGE_VALUE } from 'constant/configs';
import { createSelector } from 'reselect';
import { RootState } from 'types';
import { HomeState } from './reducer';

export const homeSelector = (state: RootState) => state.home;

const makeHomeSelector = createSelector(
  homeSelector,
  (subState: HomeState) => subState,
);

export const metamaskSelector = createSelector(
  homeSelector,
  selector => selector?.metamask ?? {},
);

export const pairDataSelector = createSelector(
  homeSelector,
  selector => selector?.pairData ?? {},
);

export const approveSelector = createSelector(
  homeSelector,
  selector => selector?.approve ?? {},
);

export const swapSelector = createSelector(
  homeSelector,
  selector => selector?.swap ?? {},
);

export const collapseSelector = createSelector(
  homeSelector,
  selector => selector?.collapsed ?? COLLAPSE_SCREEN_MAP.SWAP,
);

export const txSuccessseSelector = createSelector(
  homeSelector,
  selector => selector?.txSuccess ?? {},
);

export const slippageSelector = createSelector(
  homeSelector,
  selector => selector?.slippage ?? SLIPPAGE_VALUE[0],
);
export default makeHomeSelector;
