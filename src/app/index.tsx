/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import React, { useEffect } from 'react';
import { Helmet } from 'react-helmet-async';
import { Switch, Route, BrowserRouter } from 'react-router-dom';

import { GlobalStyle } from '../styles/global-styles';

import { HomePage } from './pages/HomePage/Loadable';
import { NotFoundPage } from './pages/NotFoundPage/Loadable';
import { useTranslation } from 'react-i18next';
import 'bootstrap/dist/css/bootstrap.min.css';
import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';
import { appName, reducer, appActions } from './pages/App/reducer';
import saga from './pages/App/saga';
import { useDispatch } from 'react-redux';
import ReactNotification from 'react-notifications-component';
import 'react-notifications-component/dist/theme.css';
import MetaMaskErrorModal from './pages/HomePage/Features/MetaMaskErrorModal';

export function App() {
  useInjectReducer({
    key: appName,
    reducer,
  });

  useInjectSaga({
    key: appName,
    saga,
  });

  const { i18n } = useTranslation();
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(appActions.initializeMetamask());
  }, [dispatch]);
  return (
    <BrowserRouter>
      <Helmet
        titleTemplate="%s - SWAP ERC20 TOKENS"
        defaultTitle="SWAP ERC20 TOKENS"
        htmlAttributes={{ lang: i18n.language }}
      >
        <meta name="description" content="A SWAP ERC20 TOKENS application" />
      </Helmet>
      <ReactNotification />
      <Switch>
        <Route exact path={process.env.PUBLIC_URL + '/'} component={HomePage} />
        <Route component={NotFoundPage} />
      </Switch>
      <MetaMaskErrorModal />
      <GlobalStyle />
    </BrowserRouter>
  );
}
