# SWAP ERC20 TOKENS
## Prerequisites

### Install Node JS(version >=12)
Refer to https://nodejs.org/en/ to install nodejs


## Cloning and Running the Application in local

⚠️ Using [Yarn Package Manager](https://yarnpkg.com) is recommended over `npm`.

Clone the project into local

Install all the npm packages. Go into the project folder and type the following command to install all npm packages

```bash
yarn install
```

In order to run the application Type the following command

```bash
yarn start
```

The Application Runs on **localhost:3000**

## Application design

#### Components

1. **Swap** Component : This Component displays a form where we have to select two currencies to which we need to perform a swap and a input for providing a amount of tokenA need to swap with tokenB

2. **Quotation** Component : This Component displays radiobox list of quotations.

3. **Navbar** Component : This Component displays connect to metamask button and info about network and balance in ETH.


#### URL

The application has just one url /customerlist which ties to *Customers* Component

## Resources

**create-react-app** : The following link has all the commands that can be used with create-react-app
https://github.com/facebook/create-react-app

**ReactJS** : Refer to https://reactjs.org/ to understand the concepts of ReactJS

**React Bootstrap** : Refer to https://react-bootstrap.github.io/getting-started/introduction/ to understand how to use React Bootstrap

## Tech Stack

- [TypeScript](https://github.com/Microsoft/TypeScript)
- [Create React App](https://github.com/facebook/create-react-app)
- [Yarn Workspaces](https://yarnpkg.com/lang/en/docs/workspaces/) _(Monorepo)_
- [React](https://github.com/facebook/react) _(100% [Hooks](https://reactjs.org/docs/hooks-intro.html), zero classes)_
- [Redux](https://github.com/reduxjs/react-redux)
- [Redux Saga](https://github.com/redux-saga/redux-saga/)
- [Reselect](https://github.com/reduxjs/reselect)
- [Web3](https://github.com/ethereum/web3.js)
- [Metamask](https://github.com/MetaMask/detect-provider)
